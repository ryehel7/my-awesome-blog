from django.apps import AppConfig


class ScobieConfig(AppConfig):
    name = 'scobie'
